GitHubDemo

O desafio consiste desenvolver um app Android nativo que tenha, basicamente, duas funcionalidades: buscar usuários e repositórios do GitHub usando sua API (que retorna JSONs com as informações desejadas).

O app deve ter ao menos duas telas, uma para busca de usuários, outra para busca de repositório, ambas devem ter um campo de busca, e listar os resultados com as devidas imagens que julgar interessante exibir (talvez a imagem de perfil do usuário).

O layout e outras telas (como exibir perfil do usuário, exibir usuários que participam do repositório), outras funcionalidades (como uma busca avançada, com mais filtros, e melhor usabilidade) ficam a critério.