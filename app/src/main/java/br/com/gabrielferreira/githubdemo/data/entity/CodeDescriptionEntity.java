package br.com.gabrielferreira.githubdemo.data.entity;

@SuppressWarnings("unused")
public class CodeDescriptionEntity {

    private String code;
    private String description;
    private String message;

    public String getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    public String getMessage() {
        return message;
    }
}
