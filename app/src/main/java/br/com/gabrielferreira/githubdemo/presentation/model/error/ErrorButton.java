package br.com.gabrielferreira.githubdemo.presentation.model.error;

/**
 * ErrorButton
 */
public class ErrorButton {
    /**
     * On button click actions
     */
    public enum Action {
        JUST_DISMISS,
        NAVIGATE_BACK,
        LOGOUT,
        CUSTOM
    }

    private String label;
    private Action action;
    private Runnable customAction;

    /**
     * Default constructor
     */
    public ErrorButton() {
        // do nothing
    }

    public String getLabel() {
        return label;
    }

    public ErrorButton withLabel(String label) {
        this.label = label;
        return this;
    }

    public Action getAction() {
        return action;
    }

    public ErrorButton withAction(Action action) {
        this.action = action;
        return this;
    }

    public Runnable getCustomAction() {
        return customAction;
    }

    public ErrorButton withCustomAction(Runnable customAction) {
        this.action = Action.CUSTOM;
        this.customAction = customAction;
        return this;
    }
}