package br.com.gabrielferreira.githubdemo.data.network.api;

import javax.inject.Inject;

import br.com.gabrielferreira.githubdemo.data.entity.search_repository.RepositoryEntity;
import br.com.gabrielferreira.githubdemo.data.network.service.SearchService;
import br.com.gabrielferreira.githubdemo.presentation.model.request.search.SearchRequest;
import retrofit2.Retrofit;
import rx.Observable;

import static br.com.gabrielferreira.githubdemo.presentation.view.util.Utils.convertObjectToMap;

public class SearchApi extends BaseApi {

    private SearchService searchService;

    @Inject
    public SearchApi() {
        Retrofit retrofit = build();
        searchService = retrofit.create(SearchService.class);
    }

    public Observable<RepositoryEntity> getRepositoryData(SearchRequest searchRequest) {
        return searchService.getRepositoryData(convertObjectToMap(searchRequest));
    }
}
