package br.com.gabrielferreira.githubdemo.data.mapper.search_repository;

import javax.inject.Inject;

import br.com.gabrielferreira.githubdemo.data.entity.search_repository.RepositoryOwnerEntity;
import br.com.gabrielferreira.githubdemo.data.mapper.BaseEntityMapper;
import br.com.gabrielferreira.githubdemo.domain.entity.search_repository.RepositoryOwner;

class RepositoryOwnerEntityMapper extends BaseEntityMapper {

    @Inject
    RepositoryOwnerEntityMapper() {

    }

    RepositoryOwner transform(final RepositoryOwnerEntity repositoryOwnerEntity) {
        return new RepositoryOwner()
                .withAvatarUrl(repositoryOwnerEntity.getAvatarUrl())
                .withId(repositoryOwnerEntity.getId())
                .withUrl(repositoryOwnerEntity.getUrl());
    }
}
