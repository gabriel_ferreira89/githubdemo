package br.com.gabrielferreira.githubdemo.presentation.mapper;

import android.content.res.Resources;

import javax.inject.Inject;


public class BaseDisplayMapper {

    protected Resources resources;

    @Inject
    public BaseDisplayMapper(Resources resources) {
        this.resources = resources;
    }

    protected String transformInt(int numberValue) {
        return String.valueOf(numberValue);
    }

    protected String getString(int resId) {
        return resources.getString(resId);
    }

    protected String getString(int resId, Object... param) {
        return resources.getString(resId, param);
    }
}

