package br.com.gabrielferreira.githubdemo.data.network.service;

import java.util.Map;

import br.com.gabrielferreira.githubdemo.data.entity.search_repository.RepositoryEntity;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;
import rx.Observable;

/**
 * SearchService
 */
public interface SearchService {

    @GET("search/repositories")
    Observable<RepositoryEntity> getRepositoryData(@QueryMap Map<String, Object> options);
}
