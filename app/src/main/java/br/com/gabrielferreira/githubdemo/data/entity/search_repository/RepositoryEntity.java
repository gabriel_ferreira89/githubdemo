package br.com.gabrielferreira.githubdemo.data.entity.search_repository;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import br.com.gabrielferreira.githubdemo.data.entity.BaseEntity;

public class RepositoryEntity extends BaseEntity {

    @SerializedName("total_count")
    @Expose
    private int totalCount;
    @SerializedName("incomplete_results")
    @Expose
    private boolean incompleteResults;
    @SerializedName("items")
    @Expose
    private List<ResultEntity> items = null;

    public int getTotalCount() {
        return totalCount;
    }

    public boolean isIncompleteResults() {
        return incompleteResults;
    }

    public List<ResultEntity> getItems() {
        return items;
    }
}
