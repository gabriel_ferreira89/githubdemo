package br.com.gabrielferreira.githubdemo.presentation.model.error;

/**
 * ErrorDisplay
 */
public class ErrorDisplay {
    private String title;
    private String message;
    private ErrorButton positiveButton;
    private ErrorButton negativeButton;

    /**
     * Default constructor
     */
    public ErrorDisplay() {
        // do nothing
    }

    public String getTitle() {
        return title;
    }

    public ErrorDisplay withTitle(String title) {
        this.title = title;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public ErrorDisplay withMessage(String message) {
        this.message = message;
        return this;
    }

    public ErrorButton getPositiveButton() {
        return positiveButton;
    }

    public ErrorDisplay withPositiveButton(ErrorButton positiveButton) {
        this.positiveButton = positiveButton;
        return this;
    }

    public ErrorButton getNegativeButton() {
        return negativeButton;
    }

    public ErrorDisplay withNegativeButton(ErrorButton negativeButton) {
        this.negativeButton = negativeButton;
        return this;
    }

}
