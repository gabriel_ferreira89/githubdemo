package br.com.gabrielferreira.githubdemo.presentation.view.listener;

/**
 * Created on 05/06/2017.
 */

public interface OnRepositoryItemClick {

    void onItemUrlClick(String url);
}
