package br.com.gabrielferreira.githubdemo.presentation.view.util;

public class Constants {

    public class Splash {
        public static final int SPLASH_TIME_OUT = 3;
    }

    public class FragmentTag {
        public static final String TAG_HOME = "TAG_HOME";
        public static final String TAG_REPOSITORY = "TAG_REPOSITORY";
        public static final String TAG_USER = "TAG_USER";
        public static final String TAG_SETTINGS = "TAG_SETTINGS";
    }
}
