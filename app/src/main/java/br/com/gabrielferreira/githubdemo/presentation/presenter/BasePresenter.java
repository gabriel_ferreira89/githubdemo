package br.com.gabrielferreira.githubdemo.presentation.presenter;


import br.com.gabrielferreira.githubdemo.presentation.mapper.error.ErrorDisplayMapper;
import br.com.gabrielferreira.githubdemo.presentation.view.BaseView;

public abstract class BasePresenter {

    protected ErrorDisplayMapper errorDisplayMapper;
    private BaseView baseView;

    public BasePresenter(ErrorDisplayMapper errorDisplayMapper) {
        this.errorDisplayMapper = errorDisplayMapper;
    }

    public void setBaseView(BaseView baseView) {
        this.baseView = baseView;
    }


    protected void showLoading() {
        if (baseView != null) {
            baseView.showLoading();
        }
    }

    protected void hideLoading() {
        if (baseView != null) {
            baseView.hideLoading();
        }
    }

    protected void showErrorToast(Throwable e) {
        if (baseView != null) {
            BasePresenter.this.hideLoading();
            baseView.showToast(errorDisplayMapper.transform(e).getMessage());
        }
    }

    public void onDestroy() {
        baseView = null;
    }
}