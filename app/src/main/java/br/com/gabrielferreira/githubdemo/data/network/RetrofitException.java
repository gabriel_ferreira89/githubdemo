package br.com.gabrielferreira.githubdemo.data.network;


import java.io.IOException;
import java.lang.annotation.Annotation;

import br.com.gabrielferreira.githubdemo.data.entity.ErrorResponse;
import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;
import timber.log.Timber;

public class RetrofitException extends RuntimeException {
    private final ErrorResponse response;
    private final Kind kind;

    private RetrofitException(String message, ErrorResponse response, Kind kind, Throwable exception) {
        super(message, exception);
        this.response = response;
        this.kind = kind;
    }

    static RetrofitException httpError(Response response, Retrofit retrofit) {
        String message = response.code() + " " + response.message();
        Converter<ResponseBody, ErrorResponse> converter = retrofit.responseBodyConverter(ErrorResponse.class, new Annotation[0]);
        try {
            return new RetrofitException(message, converter.convert(response.errorBody()), Kind.HTTP, null);
        } catch (IOException e) {
            Timber.w(e, "API returns an invalid httpError");
            return new RetrofitException(message, null, Kind.UNEXPECTED, null);
        }
    }

    static RetrofitException timeoutError(IOException exception) {
        return new RetrofitException(exception.getMessage(), null, Kind.TIME_OUT, exception);
    }

    static RetrofitException networkError(IOException exception) {
        return new RetrofitException(exception.getMessage(), null, Kind.NETWORK, exception);
    }

    static RetrofitException unexpectedError(Throwable exception) {
        return new RetrofitException(exception.getMessage(), null, Kind.UNEXPECTED, exception);
    }

    public static RetrofitException apiError(ErrorResponse errorResponse) {
        return new RetrofitException(null, errorResponse, Kind.API_ERROR, null);
    }

    static RetrofitException sessionExpiredError(ErrorResponse errorResponse) {
        return new RetrofitException(null, errorResponse, Kind.SESSION_EXPIRED, null);
    }

    public ErrorResponse getResponse() {
        return response;
    }

    /**
     * The event kind which triggered this error.
     */
    public Kind getKind() {
        return kind;
    }

    /**
     * Identifies the event kind which triggered a {@link RetrofitException}.
     */
    public enum Kind {
        /**
         * An {@link IOException} occurred while communicating to the server.
         */
        NETWORK,
        /**
         * A non-200 HTTP status code was received from the server.
         */
        HTTP,
        /**
         * An internal onAddFavouriteWithError occurred while attempting to execute a request. It is best practice to
         * re-throw this exception so your application crashes.
         */
        UNEXPECTED,
        TIME_OUT,
        API_ERROR,
        SESSION_EXPIRED
    }
}
