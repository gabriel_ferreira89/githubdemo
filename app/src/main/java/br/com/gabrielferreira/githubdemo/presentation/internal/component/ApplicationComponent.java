package br.com.gabrielferreira.githubdemo.presentation.internal.component;


import javax.inject.Singleton;

import br.com.gabrielferreira.githubdemo.presentation.view.activity.BaseActivity;
import br.com.gabrielferreira.githubdemo.presentation.view.activity.splash.SplashActivity;
import br.com.gabrielferreira.githubdemo.presentation.AndroidApplication;
import br.com.gabrielferreira.githubdemo.presentation.internal.module.ApplicationModule;
import br.com.gabrielferreira.githubdemo.presentation.view.fragment.home.HomeFragment;
import br.com.gabrielferreira.githubdemo.presentation.view.fragment.main.MainFragment;
import br.com.gabrielferreira.githubdemo.presentation.view.fragment.search_repository.SearchRepositoryFragment;
import br.com.gabrielferreira.githubdemo.presentation.view.fragment.search_user.SearchUserFragment;
import br.com.gabrielferreira.githubdemo.presentation.view.fragment.settings.SettingsFragment;
import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class})
public interface ApplicationComponent {

    // Application
    void inject(AndroidApplication androidApplication);

    //Base Activity
    void inject(BaseActivity baseActivity);

    //Splash Activity
    void inject(SplashActivity splashActivity);

    //Main Fragment
    void inject(MainFragment mainFragment);

    //SettingsFragment
    void inject(SettingsFragment settingsFragment);

    //SearchUserFragment
    void inject(SearchUserFragment searchUserFragment);

    //SearchRepositoryFragment
    void inject(SearchRepositoryFragment searchRepositoryFragment);

    //HomeFragment
    void inject(HomeFragment homeFragment);
}
