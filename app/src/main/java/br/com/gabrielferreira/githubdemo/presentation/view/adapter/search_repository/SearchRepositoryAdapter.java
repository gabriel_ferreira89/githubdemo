package br.com.gabrielferreira.githubdemo.presentation.view.adapter.search_repository;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractDraggableItemViewHolder;

import java.util.List;

import br.com.gabrielferreira.githubdemo.R;
import br.com.gabrielferreira.githubdemo.presentation.model.display.search_repository.RepositoryDisplay;
import br.com.gabrielferreira.githubdemo.presentation.view.listener.OnRepositoryItemClick;
import br.com.gabrielferreira.githubdemo.presentation.view.util.ImageUtilities;
import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchRepositoryAdapter extends RecyclerView.Adapter<SearchRepositoryAdapter.MyViewHolder> {

    private final Context mContext;
    private List<RepositoryDisplay> mList;
    private OnRepositoryItemClick listener;

    public SearchRepositoryAdapter(Context context, List<RepositoryDisplay> list, OnRepositoryItemClick listener) {
        this.mContext = context;
        this.mList = list;
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new MyViewHolder(inflater.inflate(R.layout.custom_item_search_repository, parent, false));
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.setCurrentPosition(position);

        RepositoryDisplay item = mList.get(position);

        if (item.getOwner() != null) {
            ImageUtilities.setDrawableToImageViewCenterCrop(mContext,
                    ContextCompat.getDrawable(mContext, R.drawable.shape_circle_lighter_gray_background),
                    holder.avatarImageView,
                    item.getOwner().getAvatarUrl());
        }
        holder.wikiTextView.setVisibility(item.isHasWiki() ? View.VISIBLE : View.GONE);
        holder.privateTextView.setVisibility(item.isPrivate() ? View.VISIBLE : View.GONE);
        holder.nameTextView.setText(item.getName());
        holder.descriptionTextView.setText(item.getDescription());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class MyViewHolder extends AbstractDraggableItemViewHolder {

        @BindView(R.id.repository_cell_avatar)
        ImageView avatarImageView;

        @BindView(R.id.repository_cell_wiki)
        TextView wikiTextView;

        @BindView(R.id.repository_cell_private)
        TextView privateTextView;

        @BindView(R.id.repository_cell_name)
        TextView nameTextView;

        @BindView(R.id.repository_cell_description)
        TextView descriptionTextView;

        private int currentPosition;

        MyViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, itemView);
            v.setOnClickListener(v1 -> listener.onItemUrlClick(mList.get(currentPosition).getHtmlUrl()));
        }

        void setCurrentPosition(int currentPositionParam) {
            this.currentPosition = currentPositionParam;
        }
    }
}