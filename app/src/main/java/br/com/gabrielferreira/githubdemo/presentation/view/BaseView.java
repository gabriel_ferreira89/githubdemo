package br.com.gabrielferreira.githubdemo.presentation.view;

import android.content.DialogInterface;


public interface BaseView {

    void showLoading();
    void hideLoading();
    void showToast(int resId);
    void showToast(String message);
    void showCustomDialog(String msg, String positiveMsg, String negativeMsg,
                                       DialogInterface.OnClickListener onClickListener,
                                       DialogInterface.OnClickListener negativeOnClickListener);
}
