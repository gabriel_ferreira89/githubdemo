package br.com.gabrielferreira.githubdemo.presentation.view.util;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Looper;
import android.text.TextUtils;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

import java.io.IOException;

import br.com.gabrielferreira.githubdemo.presentation.view.util.transformation.CircleTransform;
import timber.log.Timber;


public class ImageUtilities {

    private static final String URL_WITH_ERROR = "http://url.with.error";

    public static void setDrawableToImageViewCenterCrop(Context context, Drawable placeHolder, ImageView imageView, String url) {
        if (TextUtils.isEmpty(url)) {
            url = URL_WITH_ERROR;
        }

        CircleTransform transform = new CircleTransform();

        RequestCreator requestCreator = Picasso.with(context)
                .load(url)
                .transform(transform)
                .placeholder(placeHolder)
                .error(placeHolder);

        if (isMainThread()) {
            requestCreator.fit().into(imageView);
        } else {
            try {
                imageView.setImageBitmap(requestCreator.get());
            } catch (IOException e) {
                Timber.w(e, "setCircularDrawableToImageView");
            }
        }
    }

    private static boolean isMainThread() {
        return Thread.currentThread() == Looper.getMainLooper().getThread();
    }
}
