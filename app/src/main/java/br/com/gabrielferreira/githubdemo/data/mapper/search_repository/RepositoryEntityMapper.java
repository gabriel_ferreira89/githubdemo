package br.com.gabrielferreira.githubdemo.data.mapper.search_repository;

import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;

import java.util.List;

import javax.inject.Inject;

import br.com.gabrielferreira.githubdemo.data.entity.search_repository.RepositoryEntity;
import br.com.gabrielferreira.githubdemo.data.entity.search_repository.ResultEntity;
import br.com.gabrielferreira.githubdemo.data.mapper.BaseEntityMapper;
import br.com.gabrielferreira.githubdemo.domain.entity.search_repository.Repository;

public class RepositoryEntityMapper extends BaseEntityMapper {

    private RepositoryOwnerEntityMapper repositoryOwnerEntityMapper;

    @Inject
    public RepositoryEntityMapper() {
        this.repositoryOwnerEntityMapper = new RepositoryOwnerEntityMapper();
    }

    public List<Repository> transform(final RepositoryEntity repositoryEntity) {
        return Stream.of(repositoryEntity.getItems())
                .map(this::transform)
                .collect(Collectors.toList());
    }

    private Repository transform(ResultEntity resultEntity) {
        return new Repository()
                .withName(resultEntity.getFullName())
                .withHtmlUrl(resultEntity.getHtmlUrl())
                .withUrl(resultEntity.getUrl())
                .withOwner(repositoryOwnerEntityMapper.transform(resultEntity.getOwner()))
                .withDescription(resultEntity.getDescription())
                .withSize(resultEntity.getSize())
                .withHasWiki(resultEntity.isHasWiki())
                .withPrivate(resultEntity.isPrivate());
    }
}
