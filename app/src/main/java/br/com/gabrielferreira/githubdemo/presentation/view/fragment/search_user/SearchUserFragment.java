package br.com.gabrielferreira.githubdemo.presentation.view.fragment.search_user;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.gabrielferreira.githubdemo.R;
import br.com.gabrielferreira.githubdemo.presentation.view.fragment.BaseFragment;
import butterknife.ButterKnife;


public class SearchUserFragment extends BaseFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_search_user, container, false);
        ButterKnife.bind(this, view);
        getApplicationComponent().inject(this);
        return view;
    }

}
