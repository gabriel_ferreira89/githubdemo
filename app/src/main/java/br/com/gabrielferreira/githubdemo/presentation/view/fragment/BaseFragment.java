package br.com.gabrielferreira.githubdemo.presentation.view.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatDialog;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import br.com.gabrielferreira.githubdemo.R;
import br.com.gabrielferreira.githubdemo.presentation.internal.component.ApplicationComponent;
import br.com.gabrielferreira.githubdemo.presentation.view.BaseView;
import br.com.gabrielferreira.githubdemo.presentation.view.activity.BaseActivity;
import butterknife.BindView;
import rx.Subscription;

/**
 * Created on 29/05/2017.
 */

public abstract class BaseFragment extends Fragment implements BaseView {

    @Nullable
    @BindView(R.id.view_group_progress)
    protected ViewGroup mProgress;

    @Nullable
    @BindView(R.id.toolbar)
    protected Toolbar mToolbar;

    @Nullable
    @BindView(R.id.title_text_view)
    protected TextView mActionBarTitle;


    protected ActionBar mActionBar;
    protected Subscription subscription;
    private AppCompatDialog mCustomDialog;

    protected ApplicationComponent getApplicationComponent() {
        return ((BaseActivity) getActivity()).getApplicationComponent();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setHasOptionsMenu(true);
    }

    @Override
    public void onDestroy() {
        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }

        if (mCustomDialog != null && mCustomDialog.isShowing()) {
            mCustomDialog.dismiss();
        }

        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == android.R.id.home) {
            if (getActivity() != null) {
                getActivity().onBackPressed();
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    protected void setupActionBar() {
        if (mToolbar != null) {
            BaseActivity baseActivity = null;

            if (getActivity() instanceof BaseActivity) {
                baseActivity = (BaseActivity) getActivity();
            }

            if (mToolbar != null && baseActivity != null) {
                baseActivity.setSupportActionBar(mToolbar);
                mActionBar = baseActivity.getSupportActionBar();

                if (mActionBar != null) {
                    mActionBar.setTitle("");
                    mActionBar.setDisplayHomeAsUpEnabled(false);
                }
            }
        } else if (getActivity() != null) {
            ((BaseActivity) getActivity()).setupActionBar();
        }
    }

    @Override
    public void showLoading() {
        if (mProgress != null) {
            mProgress.setVisibility(View.VISIBLE);
        } else if (getActivity() != null) {
            ((BaseActivity) getActivity()).showLoading();
        }
    }

    @Override
    public void hideLoading() {
        if (mProgress != null) {
            mProgress.setVisibility(View.GONE);
        } else if (getActivity() != null) {
            ((BaseActivity) getActivity()).hideLoading();
        }
    }

    @Override
    public void showToast(int resId) {
        if (getActivity() != null) {
            ((BaseActivity) getActivity()).showToast(resId);
        }
    }

    @Override
    public void showToast(String message) {
        if (getActivity() != null) {
            ((BaseActivity) getActivity()).showToast(message);
        }
    }

    @Override
    public void showCustomDialog(String msg, String positiveMsg, String negativeMsg, DialogInterface.OnClickListener onClickListener, DialogInterface.OnClickListener negativeOnClickListener) {
        if (getActivity() != null) {
            ((BaseActivity) getActivity()).showCustomDialog(msg, positiveMsg, negativeMsg, onClickListener, negativeOnClickListener);
        }
    }

    protected void setTitle(String title) {
        if (mActionBarTitle != null) {
            mActionBarTitle.setText(title);
        } else if (getActivity() != null) {
            ((BaseActivity) getActivity()).setTitle(title);
        }
    }
}
