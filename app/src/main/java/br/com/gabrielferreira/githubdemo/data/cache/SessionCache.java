package br.com.gabrielferreira.githubdemo.data.cache;

import javax.inject.Inject;

/**
 * SessionCache
 */
public class SessionCache {

    /**
     * DI constructor
     */
    @Inject
    public SessionCache() {
        // no use cases
    }
}