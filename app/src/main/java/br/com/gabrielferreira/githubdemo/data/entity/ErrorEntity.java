package br.com.gabrielferreira.githubdemo.data.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("unused")
public class ErrorEntity implements Serializable{

    private String message;
    private String code;

    @SerializedName("return")
    private String cause;

    public static ErrorEntity create(){
        return new ErrorEntity();
    }

    public ErrorEntity withMessage(String message) {
        this.message = message;
        return this;
    }

    public ErrorEntity withCode(String code) {
        this.code = code;
        return this;
    }

    public ErrorEntity withCause(String cause) {
        this.cause = cause;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public String getCode() {
        return code;
    }

    public String getCause() {
        return cause;
    }
}
