package br.com.gabrielferreira.githubdemo.presentation.view;

import java.util.List;

import br.com.gabrielferreira.githubdemo.presentation.model.display.search_repository.RepositoryDisplay;

public interface SearchRepositoryView extends BaseView {

    void setRecyclerView(List<RepositoryDisplay> repositoryDisplayList);
}
