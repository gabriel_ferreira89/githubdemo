package br.com.gabrielferreira.githubdemo.presentation;

import android.support.multidex.MultiDexApplication;

import com.crashlytics.android.Crashlytics;

import br.com.gabrielferreira.githubdemo.BuildConfig;
import br.com.gabrielferreira.githubdemo.presentation.internal.component.ApplicationComponent;
import br.com.gabrielferreira.githubdemo.presentation.internal.component.DaggerApplicationComponent;
import br.com.gabrielferreira.githubdemo.presentation.internal.module.ApplicationModule;
import io.fabric.sdk.android.Fabric;
import timber.log.Timber;

public class AndroidApplication extends MultiDexApplication {

    private ApplicationComponent mApplicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        initializeInjector();
        getApplicationComponent().inject(this);

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        } else {
            // Do not enable fabric for debug build
            Timber.i("Enter Fabric");
            Fabric fabric = new Fabric.Builder(this).kits(new Crashlytics()).appIdentifier(BuildConfig.APPLICATION_ID).build();
            Fabric.with(fabric);
        }

        Timber.d(getApplicationContext().getPackageName());
    }
    private void initializeInjector() {
        this.mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return this.mApplicationComponent;
    }

}