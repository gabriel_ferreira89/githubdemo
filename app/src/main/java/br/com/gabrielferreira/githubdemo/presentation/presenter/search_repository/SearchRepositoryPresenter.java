package br.com.gabrielferreira.githubdemo.presentation.presenter.search_repository;

import android.support.annotation.Nullable;

import java.util.List;

import javax.inject.Inject;

import br.com.gabrielferreira.githubdemo.domain.entity.search_repository.Repository;
import br.com.gabrielferreira.githubdemo.domain.interactor.SearchRepositoryInteractor;
import br.com.gabrielferreira.githubdemo.presentation.mapper.error.ErrorDisplayMapper;
import br.com.gabrielferreira.githubdemo.presentation.mapper.search_repository.RepositoryItemMapper;
import br.com.gabrielferreira.githubdemo.presentation.model.request.search.SearchRequest;
import br.com.gabrielferreira.githubdemo.presentation.presenter.BasePresenter;
import br.com.gabrielferreira.githubdemo.presentation.view.SearchRepositoryView;
import rx.Subscriber;


public class SearchRepositoryPresenter extends BasePresenter {

    private final SearchRepositoryInteractor searchRepositoryInteractor;
    private RepositoryItemMapper repositoryItemMapper;

    @Nullable
    private SearchRepositoryView searchRepositoryView;

    @Inject
    SearchRepositoryPresenter(ErrorDisplayMapper errorDisplayMapper,
                              SearchRepositoryInteractor searchRepositoryInteractor,
                              RepositoryItemMapper repositoryItemMapper) {
        super(errorDisplayMapper);
        this.searchRepositoryInteractor = searchRepositoryInteractor;
        this.repositoryItemMapper = repositoryItemMapper;
    }

    public void setView(SearchRepositoryView searchRepositoryView) {
        setBaseView(searchRepositoryView);
        this.searchRepositoryView = searchRepositoryView;
    }

    public void requestRepositoryData(String query) {
        SearchRepositoryPresenter.this.showLoading();
        this.searchRepositoryInteractor.getRepositoryData(new SearchRequest().withSearchKeywords(query));
        this.searchRepositoryInteractor.execute(new SearchRepositoryPresenter.SearchRepositorySubscriber());
    }

    private final class SearchRepositorySubscriber extends Subscriber<List<Repository>> {

        @Override
        public void onCompleted() {
            SearchRepositoryPresenter.this.hideLoading();
        }

        @Override
        public void onError(Throwable e) {
            SearchRepositoryPresenter.this.showErrorToast(e);
        }

        @Override
        public void onNext(List<Repository> repositoryList) {
            if (searchRepositoryView != null) {
                searchRepositoryView.setRecyclerView(repositoryItemMapper.transform(repositoryList));
            }
        }
    }
}
