package br.com.gabrielferreira.githubdemo.data.network.api;

import android.support.annotation.NonNull;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import br.com.gabrielferreira.githubdemo.BuildConfig;
import br.com.gabrielferreira.githubdemo.data.network.RxErrorHandlingCallAdapterFactory;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.CallAdapter;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

abstract class BaseApi {

    private CallAdapter.Factory callAdapterFactory;
    private static final int API_TIMEOUT = 30;

    BaseApi() {
        this.callAdapterFactory = RxErrorHandlingCallAdapterFactory.create();
    }

    Retrofit build() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .addInterceptor(generalInterceptor());

        OkHttpClient.Builder clientBuilder = builder
                .readTimeout(API_TIMEOUT, TimeUnit.SECONDS)
                .connectTimeout(API_TIMEOUT, TimeUnit.SECONDS);

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            clientBuilder.addInterceptor(loggingInterceptor);
        }

        OkHttpClient client = clientBuilder.build();

        Retrofit.Builder retrofitBuilder = new Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .addConverterFactory(defaultConverterFactory())
                .client(client);

        if (callAdapterFactory != null) {
            retrofitBuilder.addCallAdapterFactory(callAdapterFactory);
        }

        return retrofitBuilder.build();
    }

    @NonNull
    private Interceptor generalInterceptor() {
        return chain -> {
            Request original = chain.request();
            Request.Builder builder = original.newBuilder()
                    .header("Content-Type", "application/json")
                    .method(original.method(), original.body());
            Request request = builder.build();
            return chain.proceed(request);
        };
    }

    @NonNull
    private Converter.Factory defaultConverterFactory() {
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MMM-dd'T'HH:mm:ss.SSSZ")
                .setFieldNamingPolicy(FieldNamingPolicy.IDENTITY)
                .create();
        return GsonConverterFactory.create(gson);
    }
}
