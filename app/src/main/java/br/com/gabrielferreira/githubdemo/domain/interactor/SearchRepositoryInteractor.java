package br.com.gabrielferreira.githubdemo.domain.interactor;

import javax.inject.Inject;

import br.com.gabrielferreira.githubdemo.data.repository.SearchRepository;
import br.com.gabrielferreira.githubdemo.domain.executor.ThreadExecutor;
import br.com.gabrielferreira.githubdemo.presentation.model.request.search.SearchRequest;
import rx.Observable;

public class SearchRepositoryInteractor extends BaseInteractor {

    private SearchRepository searchRepository;

    @Inject
    public SearchRepositoryInteractor(
            final ThreadExecutor subscriberOn,
            final ThreadExecutor observerOn,
            SearchRepository searchRepository) {
        super(subscriberOn, observerOn);
        this.searchRepository = searchRepository;
    }

    public Observable getRepositoryData(SearchRequest request) {
        return withObservable(searchRepository.getRepositoryData(request));
    }
}
