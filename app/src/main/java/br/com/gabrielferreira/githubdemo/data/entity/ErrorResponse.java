package br.com.gabrielferreira.githubdemo.data.entity;

import java.io.Serializable;

public class ErrorResponse implements Serializable {

    private ErrorEntity error;

    public ErrorEntity getError() {
        return error;
    }

    public static ErrorResponse create() {
        return new ErrorResponse();
    }

    public ErrorResponse withError(ErrorEntity errorEntity) {
        this.error = errorEntity;
        return this;
    }
}
