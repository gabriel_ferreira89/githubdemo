package br.com.gabrielferreira.githubdemo.presentation.mapper.search_repository;

import android.content.res.Resources;

import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;

import java.util.List;

import javax.inject.Inject;

import br.com.gabrielferreira.githubdemo.domain.entity.search_repository.Repository;
import br.com.gabrielferreira.githubdemo.presentation.mapper.BaseDisplayMapper;
import br.com.gabrielferreira.githubdemo.presentation.model.display.search_repository.RepositoryDisplay;


public class RepositoryItemMapper extends BaseDisplayMapper {

    private RepositoryOwnerItemMapper repositoryOwnerItemMapper;

    @Inject
    RepositoryItemMapper(Resources resources, RepositoryOwnerItemMapper repositoryOwnerItemMapper) {
        super(resources);
        this.repositoryOwnerItemMapper = repositoryOwnerItemMapper;
    }

    public List<RepositoryDisplay> transform(List<Repository> repositoryList) {
        return Stream.of(repositoryList)
                .map(this::transform)
                .collect(Collectors.toList());
    }

    private RepositoryDisplay transform(Repository repository){
        return new RepositoryDisplay()
                .withName(repository.getName())
                .withHtmlUrl(repository.getHtmlUrl())
                .withUrl(repository.getUrl())
                .withOwner(repositoryOwnerItemMapper.transform(repository.getOwner()))
                .withDescription(repository.getDescription())
                .withSize(repository.getSize())
                .withHasWiki(repository.isHasWiki())
                .withIsPrivate(repository.isPrivate());
    }

}
