package br.com.gabrielferreira.githubdemo.presentation.internal.module;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.content.res.Resources;

import java.io.File;

import javax.inject.Named;
import javax.inject.Singleton;

import br.com.gabrielferreira.githubdemo.BuildConfig;
import br.com.gabrielferreira.githubdemo.data.cache.SessionCache;
import br.com.gabrielferreira.githubdemo.data.network.api.SearchApi;
import br.com.gabrielferreira.githubdemo.data.network.api.SearchApiMock;
import br.com.gabrielferreira.githubdemo.data.repository.SearchRepository;
import br.com.gabrielferreira.githubdemo.domain.executor.ThreadExecutor;
import br.com.gabrielferreira.githubdemo.domain.interactor.SearchRepositoryInteractor;
import br.com.gabrielferreira.githubdemo.presentation.AndroidApplication;
import dagger.Module;
import dagger.Provides;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

@Module
public class ApplicationModule {

    private final AndroidApplication mApplication;

    public ApplicationModule(AndroidApplication application) {
        this.mApplication = application;
    }

    // Resources provide

    @Provides
    Context provideApplicationContext() {
        return mApplication.getApplicationContext();
    }

    @Provides
    Resources provideResources(Context context) {
        return context.getResources();
    }

    @Provides
    AssetManager provideAssets(Context context) {
        return context.getAssets();
    }

    // Cache provide

    @Provides
    @Singleton
    SharedPreferences provideCacheRepository(final Context context) {
        return context.getSharedPreferences(BuildConfig.APPLICATION_ID, Context.MODE_PRIVATE);
    }

    @Provides
    @Singleton
    File provideCacheDir() {
        return mApplication.getFilesDir();
    }

    @Provides
    @Singleton
    SessionCache provideSessionCache() {
        return new SessionCache();
    }

    // ThreadExecutor provide
    @Provides
    @Singleton
    @Named("subscriberOn")
    ThreadExecutor provideSubscriberOnThreadExecutor() {
        return new ThreadExecutor(Schedulers.newThread());
    }

    @Provides
    @Singleton
    @Named("observerOn")
    ThreadExecutor provideObserverOnExecutionThread() {
        return new ThreadExecutor(AndroidSchedulers.mainThread());
    }

    // Repository provide
    @Provides
    @Singleton
    SearchRepository provideSearchRepository(SearchApi searchApi) {
        return new SearchRepository(searchApi);
    }


    // Api provide
    @Provides
    @Singleton
    SearchApi provideSearchApi() {
        if (BuildConfig.USE_MOCK) {
            return new SearchApiMock();
        }
        return new SearchApi();
    }

    // Interactor provide
    @Provides
    @Singleton
    SearchRepositoryInteractor provideSearchRepositoryInteractor(@Named("subscriberOn") final ThreadExecutor subscriberOn,
                                                                 @Named("observerOn") final ThreadExecutor observerOn,
                                                                 final SearchRepository searchRepository) {
        return new SearchRepositoryInteractor(subscriberOn, observerOn, searchRepository);
    }

}