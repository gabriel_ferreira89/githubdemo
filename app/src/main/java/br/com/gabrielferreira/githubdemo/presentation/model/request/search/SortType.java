package br.com.gabrielferreira.githubdemo.presentation.model.request.search;

import com.google.gson.annotations.SerializedName;

public enum SortType {

    @SerializedName("stars")
    STARS,
    @SerializedName("forks")
    FORKS,
    @SerializedName("updated")
    UPDATED;

    @Override
    public String toString() {
        return this.name();
    }
}
