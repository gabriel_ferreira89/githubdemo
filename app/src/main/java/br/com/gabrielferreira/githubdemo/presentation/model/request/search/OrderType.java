package br.com.gabrielferreira.githubdemo.presentation.model.request.search;

import com.google.gson.annotations.SerializedName;

public enum OrderType {

    @SerializedName("asc")
    ASCENDING,
    @SerializedName("desc")
    DESCENDING;

    @Override
    public String toString() {
        return this.name().toLowerCase();
    }
}
