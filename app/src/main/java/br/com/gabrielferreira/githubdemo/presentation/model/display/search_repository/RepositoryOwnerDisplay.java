package br.com.gabrielferreira.githubdemo.presentation.model.display.search_repository;

import br.com.gabrielferreira.githubdemo.presentation.model.display.BaseDisplay;

/**
 * Created on 03/06/2017.
 */

public class RepositoryOwnerDisplay extends BaseDisplay {

    private String avatarUrl;
    private long id;
    private String url;

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public RepositoryOwnerDisplay withAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
        return this;
    }

    public long getId() {
        return id;
    }

    public RepositoryOwnerDisplay withId(long id) {
        this.id = id;
        return this;
    }

    public String getUrl() {
        return url;
    }

    public RepositoryOwnerDisplay withUrl(String url) {
        this.url = url;
        return this;
    }
}
