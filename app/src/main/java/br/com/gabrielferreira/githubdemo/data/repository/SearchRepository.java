package br.com.gabrielferreira.githubdemo.data.repository;


import java.util.List;

import javax.inject.Inject;

import br.com.gabrielferreira.githubdemo.data.mapper.search_repository.RepositoryEntityMapper;
import br.com.gabrielferreira.githubdemo.data.network.api.SearchApi;
import br.com.gabrielferreira.githubdemo.domain.entity.search_repository.Repository;
import br.com.gabrielferreira.githubdemo.presentation.model.request.search.SearchRequest;
import rx.Observable;

public class SearchRepository {

    private final SearchApi searchApi;
    private final RepositoryEntityMapper repositoryEntityMapper;

    @Inject
    public SearchRepository(SearchApi searchApi) {
        this.searchApi = searchApi;
        this.repositoryEntityMapper = new RepositoryEntityMapper();
    }

    public Observable<List<Repository>> getRepositoryData(SearchRequest request) {
        return searchApi.getRepositoryData(request)
                .flatMap(repositoryEntity -> Observable.just(repositoryEntityMapper.transform(repositoryEntity)));
    }
}
