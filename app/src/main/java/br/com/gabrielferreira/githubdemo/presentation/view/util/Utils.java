package br.com.gabrielferreira.githubdemo.presentation.view.util;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Map;

public class Utils {

    public static Map<String, Object> convertObjectToMap(Object obj) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        JsonElement jsonElement = gsonBuilder.create().toJsonTree(obj);
        Type type = new TypeToken<Map<String, String>>(){}.getType();
        return gsonBuilder.create().fromJson(jsonElement, type);
    }
}
