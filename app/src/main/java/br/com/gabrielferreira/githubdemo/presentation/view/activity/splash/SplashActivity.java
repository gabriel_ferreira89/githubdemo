package br.com.gabrielferreira.githubdemo.presentation.view.activity.splash;

import android.os.Bundle;

import com.crashlytics.android.Crashlytics;

import br.com.gabrielferreira.githubdemo.R;
import br.com.gabrielferreira.githubdemo.presentation.view.activity.BaseActivity;
import io.fabric.sdk.android.Fabric;

public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_splash);
    }
}
