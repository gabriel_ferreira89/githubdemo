package br.com.gabrielferreira.githubdemo.presentation.model.display.search_repository;

import br.com.gabrielferreira.githubdemo.presentation.model.display.BaseDisplay;


public class RepositoryDisplay extends BaseDisplay {

    private String name;
    private String htmlUrl;
    private String url;
    private RepositoryOwnerDisplay owner;
    private String description;
    private long size;
    private boolean hasWiki;
    private boolean isPrivate;

    public String getName() {
        return name;
    }

    public RepositoryDisplay withName(String name) {
        this.name = name;
        return this;
    }

    public String getHtmlUrl() {
        return htmlUrl;
    }

    public RepositoryDisplay withHtmlUrl(String htmlUrl) {
        this.htmlUrl = htmlUrl;
        return this;
    }

    public String getUrl() {
        return url;
    }

    public RepositoryDisplay withUrl(String url) {
        this.url = url;
        return this;
    }

    public RepositoryOwnerDisplay getOwner() {
        return owner;
    }

    public RepositoryDisplay withOwner(RepositoryOwnerDisplay owner) {
        this.owner = owner;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public RepositoryDisplay withDescription(String description) {
        this.description = description;
        return this;
    }

    public long getSize() {
        return size;
    }

    public RepositoryDisplay withSize(long size) {
        this.size = size;
        return this;
    }

    public boolean isHasWiki() {
        return hasWiki;
    }

    public RepositoryDisplay withHasWiki(boolean hasWiki) {
        this.hasWiki = hasWiki;
        return this;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public RepositoryDisplay withIsPrivate(boolean aPrivate) {
        isPrivate = aPrivate;
        return this;
    }
}
