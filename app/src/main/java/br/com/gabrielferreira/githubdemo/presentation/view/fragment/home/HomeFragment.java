package br.com.gabrielferreira.githubdemo.presentation.view.fragment.home;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.gabrielferreira.githubdemo.R;
import br.com.gabrielferreira.githubdemo.presentation.view.fragment.BaseFragment;
import butterknife.ButterKnife;

public class HomeFragment extends BaseFragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);
        getApplicationComponent().inject(this);
        return view;
    }

}
