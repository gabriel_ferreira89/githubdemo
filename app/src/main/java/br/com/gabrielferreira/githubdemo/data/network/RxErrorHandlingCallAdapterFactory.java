package br.com.gabrielferreira.githubdemo.data.network;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.net.SocketTimeoutException;

import br.com.gabrielferreira.githubdemo.data.entity.BaseEntity;
import br.com.gabrielferreira.githubdemo.data.entity.CodeDescriptionEntity;
import br.com.gabrielferreira.githubdemo.data.entity.ErrorEntity;
import retrofit2.Call;
import retrofit2.CallAdapter;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.HttpException;
import retrofit2.adapter.rxjava.Result;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import rx.Observable;
import rx.functions.Func1;

/**
 * RxErrorHandlingCallAdapterFactory
 */
public class RxErrorHandlingCallAdapterFactory extends CallAdapter.Factory {

    private final RxJavaCallAdapterFactory original;

    private RxErrorHandlingCallAdapterFactory() {
        original = RxJavaCallAdapterFactory.create();
    }

    /**
     * Create an instance of RxErrorHandlingCallAdapterFactory
     *
     * @return a factory object
     */
    public static CallAdapter.Factory create() {
        return new RxErrorHandlingCallAdapterFactory();
    }

    @Override
    public CallAdapter<?> get(Type returnType, Annotation[] annotations, Retrofit retrofit) {
        return new RxCallAdapterWrapper(retrofit, original.get(returnType, annotations, retrofit));
    }

    private static class RxCallAdapterWrapper implements CallAdapter<Observable<?>> {
        private final Retrofit retrofit;
        private final CallAdapter<?> wrapped;

        RxCallAdapterWrapper(Retrofit retrofit, CallAdapter<?> wrapped) {
            this.retrofit = retrofit;
            this.wrapped = wrapped;
        }

        @Override
        public Type responseType() {
            return wrapped.responseType();
        }

        @SuppressWarnings("unchecked")
        @Override
        public <R> Observable<?> adapt(Call<R> call) {
            return ((Observable) wrapped.adapt(call)).doOnNext(obj -> {
                if (obj instanceof BaseEntity) {
                    processError((BaseEntity) obj);
                } else if(obj instanceof Result && ((Result) obj).isError()) {
                    throw asRetrofitException(retrofit, ((Result) obj).error());
                }
            }).onErrorResumeNext(new Func1<Throwable, Observable>() {
                @Override
                public Observable call(Throwable throwable) {
                    return Observable.error(asRetrofitException(retrofit, throwable));
                }
            });
        }
    }

    /**
     * Create RetrofitException from a Throwable
     *
     * @param retrofit  Retrofit
     * @param throwable Throwable
     * @return RetrofitException
     */
    private static RetrofitException asRetrofitException(Retrofit retrofit, Throwable throwable) {
        // We had non-200 http onAddFavouriteWithError
        if (throwable instanceof HttpException) {
            HttpException httpException = (HttpException) throwable;
            Response response = httpException.response();
            return RetrofitException.httpError(response, retrofit);
        }
        // A network error happened
        if (throwable instanceof SocketTimeoutException) {
            return RetrofitException.timeoutError((SocketTimeoutException) throwable);
        }
        // A network error happened
        if (throwable instanceof IOException) {
            return RetrofitException.networkError((IOException) throwable);
        }
        if (throwable instanceof RetrofitException) {
            return (RetrofitException) throwable;
        }
        // We don't know what happened. We need to simply convert to an unknown onAddFavouriteWithError
        return RetrofitException.unexpectedError(throwable);
    }

    /**
     * Check an error in the entity, wrap, and throw
     *
     * @param baseEntity BaseEntity
     */
    public static void processError(BaseEntity baseEntity) {
        if (baseEntity.getError() != null) {
            throwErrorException(baseEntity.getError());
        } else if (baseEntity.getStatus() != null
                && baseEntity.getStatus().getCode() != null) {
            throwErrorException(baseEntity.getStatus());
        }
    }

    private static void throwErrorException(CodeDescriptionEntity codeDescriptionEntity) {
        ErrorEntity errorEntity = ErrorEntity.create();
        try {
            errorEntity.withCode(codeDescriptionEntity.getCode())
                    .withMessage(codeDescriptionEntity.getMessage() != null ? codeDescriptionEntity.getMessage() : codeDescriptionEntity.getDescription());
        } catch (Exception ex) {
            throw RetrofitException.unexpectedError(ex);
        }
    }

}
