package br.com.gabrielferreira.githubdemo.domain.interactor;


import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;

import java.util.ArrayList;
import java.util.List;

import br.com.gabrielferreira.githubdemo.domain.executor.ThreadExecutor;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.functions.Action1;
import timber.log.Timber;

/**
 * BaseInteractor
 */
public abstract class BaseInteractor {

    protected ThreadExecutor subscriberOn;
    protected ThreadExecutor observerOn;

    private List<Subscription> subscriptions = new ArrayList<>();
    private Observable observable;

    /**
     * Constructor
     *
     * @param subscriberOn ThreadExecutor
     * @param observerOn   ThreadExecutor
     */
    public BaseInteractor(final ThreadExecutor subscriberOn, final ThreadExecutor observerOn) {
        this.subscriptions = new ArrayList<>();
        this.subscriberOn = subscriberOn;
        this.observerOn = observerOn;
    }

    protected Observable withObservable(final Observable observable) {
        this.observable = observable;
        return this.observable;
    }

    /**
     * Subscribe the subscriber to the registered observable
     *
     * @param subscriber Subscriber
     */
    public void execute(final Subscriber subscriber) {
        getSubscription(observable, subscriber);
    }

    // Another options for calling execute. Just to make the code for the caller be more expressive.

    /**
     * Subscribe the subscriber to the observable
     *
     * @param observable Observable
     * @param subscriber Subscriber
     * @param <T>        Observable type
     */
    public <T> void execute(Observable<T> observable, Subscriber<T> subscriber) {
        getSubscription(observable, subscriber);
    }

    // Another options for calling execute. Just to make the code for the caller be more expressive.

    /**
     * Subscribe the onNext action to the observable
     *
     * @param observable Observable
     * @param onNext     onNext Action
     * @param <T>        Observable type
     */
    public <T> void execute(Observable<T> observable, Action1<T> onNext) {
        getSubscription(observable, onNext, Timber::w);
    }

    /**
     * Subscribe onNext and onError actions to the observable
     *
     * @param observable Observable
     * @param onNext     onNext Action
     * @param onError    onError Action
     * @param <T>        Observable type
     */
    public <T> void execute(Observable<T> observable, Action1<T> onNext, Action1<Throwable> onError) {
        getSubscription(observable, onNext, onError);
    }

    /**
     * Unsubscribe all
     */
    public void unsubscribe() {
        unsubscribeAll();
    }

    /**
     * Unsubscribe all
     */
    public void unsubscribeAll() {
        if (subscriptions == null) {
            return;
        }

        Stream.of(subscriptions)
                .filter(item -> !item.isUnsubscribed())
                .forEach(Subscription::unsubscribe);

        subscriptions.clear();
    }

    private <T> Subscription getSubscription(Observable<T> observable, final Action1<T> onNext, final Action1<Throwable> onError) {
        Subscription subscription = observable
                .subscribeOn(subscriberOn.getScheduler())
                .observeOn(observerOn.getScheduler())
                .subscribe(onNext, onError);

        addSubscription(subscription);

        return subscription;
    }

    private <T> Subscription getSubscription(Observable<T> observable, final Subscriber<T> subscriber) {
        Subscription subscription = observable
                .subscribeOn(subscriberOn.getScheduler())
                .observeOn(observerOn.getScheduler())
                .subscribe(subscriber);

        addSubscription(subscription);

        return subscription;
    }

    private void addSubscription(Subscription subscription) {
        if (subscription != null) {
            subscriptions.removeAll(Stream.of(subscriptions).filter(Subscription::isUnsubscribed).collect(Collectors.toList()));
            subscriptions.add(subscription);
        }
    }


}
