package br.com.gabrielferreira.githubdemo.presentation.view.util.transformation;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;

import com.squareup.picasso.Transformation;

// Credit: http://stackoverflow.com/a/26112408
// Credit: https://gist.github.com/opticod/afec8dc1db52e3b04570f332ac0e09ba
public class CircleTransform implements Transformation {

    private float margin;

    public CircleTransform() {
        this(0F);
    }

    public CircleTransform(float margin) {
        this.margin = margin;
    }

    @Override
    public Bitmap transform(Bitmap source) {
        int size = Math.min(source.getWidth(), source.getHeight());

        int x = (source.getWidth() - size) / 2;
        int y = (source.getHeight() - size) / 2;

        Bitmap squaredBitmap = Bitmap.createBitmap(source, x, y, size, size);
        if (squaredBitmap != source) {
            source.recycle();
        }

        Bitmap bitmap = Bitmap.createBitmap(size, size, squaredBitmap.getConfig());

        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint();
        BitmapShader shader = new BitmapShader(squaredBitmap, BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
        paint.setShader(shader);
        paint.setAntiAlias(true);

        float r = size / 2f;

        canvas.drawRoundRect(new RectF(margin, margin, squaredBitmap.getWidth() - margin, squaredBitmap.getHeight() - margin), r, r, paint);

        if(bitmap != squaredBitmap) {
            squaredBitmap.recycle();
        }
        return bitmap;
    }

    @Override
    public String key() {
        return "circle";
    }
}