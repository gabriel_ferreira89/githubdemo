package br.com.gabrielferreira.githubdemo.presentation.mapper.error;

import android.content.res.Resources;
import android.text.TextUtils;

import javax.inject.Inject;

import br.com.gabrielferreira.githubdemo.R;
import br.com.gabrielferreira.githubdemo.data.network.RetrofitException;
import br.com.gabrielferreira.githubdemo.presentation.mapper.BaseDisplayMapper;
import br.com.gabrielferreira.githubdemo.presentation.model.display.error.ErrorDisplay;
import br.com.gabrielferreira.githubdemo.presentation.model.error.ErrorButton;

/**
 * ErrorDisplayMapper
 */
public class ErrorDisplayMapper extends BaseDisplayMapper {

    @Inject
    ErrorDisplayMapper(Resources resources) {
        super(resources);
    }

    /**
     * Produce simple ErrorDisplay from Throwable
     *
     * @param throwable Throwable
     * @return ErrorDisplay
     */
    public ErrorDisplay transform(Throwable throwable, ErrorButton.Action action) {
        String message = null;

        if (throwable instanceof RetrofitException) {
            RetrofitException retrofitException = (RetrofitException) throwable;
            switch (retrofitException.getKind()) {
                case HTTP:
                case API_ERROR:
                    try {
                        message = retrofitException.getResponse().getError().getMessage();
                    } catch (Exception ex) {
                        message = null;
                    }
                    break;
                case NETWORK:
                    message = getString(R.string.error_connection);
                    break;
                case TIME_OUT:
                    message = getString(R.string.error_generic);
                    break;
                case UNEXPECTED:
                    message = null;
                    break;
                case SESSION_EXPIRED:
                    action = ErrorButton.Action.LOGOUT;
                    try {
                        message = retrofitException.getResponse().getError().getMessage();
                    } catch (Exception ex) {
                        message = getString(R.string.session_expired);
                    }
                    break;
                default:
                    message = null;
                    break;
            }
        }

        return transform(!TextUtils.isEmpty(message) ? message : getString(R.string.error_generic_resource), action);
    }

    public ErrorDisplay transform(Throwable throwable) {
        return transform(throwable, ErrorButton.Action.JUST_DISMISS);
    }

    public ErrorDisplay transform(String message, ErrorButton.Action action) {
        String title = getString(R.string.common_error);

        String positiveLabel = getString(R.string.ok);

        ErrorButton positiveButton = new ErrorButton()
                .withLabel(positiveLabel)
                .withAction(action);

        return new ErrorDisplay()
                .withTitle(title)
                .withMessage(message)
                .withPositiveButton(positiveButton);
    }
}
