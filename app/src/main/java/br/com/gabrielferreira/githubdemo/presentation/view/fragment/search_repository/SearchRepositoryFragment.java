package br.com.gabrielferreira.githubdemo.presentation.view.fragment.search_repository;


import android.app.SearchManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import java.util.List;

import javax.inject.Inject;

import br.com.gabrielferreira.githubdemo.R;
import br.com.gabrielferreira.githubdemo.presentation.model.display.search_repository.RepositoryDisplay;
import br.com.gabrielferreira.githubdemo.presentation.presenter.search_repository.SearchRepositoryPresenter;
import br.com.gabrielferreira.githubdemo.presentation.view.SearchRepositoryView;
import br.com.gabrielferreira.githubdemo.presentation.view.adapter.search_repository.SearchRepositoryAdapter;
import br.com.gabrielferreira.githubdemo.presentation.view.fragment.BaseFragment;
import br.com.gabrielferreira.githubdemo.presentation.view.listener.OnRepositoryItemClick;
import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.Context.SEARCH_SERVICE;

public class SearchRepositoryFragment extends BaseFragment implements SearchRepositoryView, SearchView.OnQueryTextListener, OnRepositoryItemClick {

    @Inject
    SearchRepositoryPresenter presenter;

    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

    @BindView(R.id.empty_list_card)
    RelativeLayout emptyListCard;

    private View view;
    private SearchRepositoryAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private SearchView searchView;
    private SearchManager searchManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (view != null) {
            return view;
        }
        view = inflater.inflate(R.layout.fragment_search_repository, container, false);
        ButterKnife.bind(this, view);
        setupActionBar();
        getApplicationComponent().inject(this);
        presenter.setView(this);
        return view;
    }

    @Override
    public void setRecyclerView(List<RepositoryDisplay> repositoryDisplayList) {
        emptyListCard.setVisibility(repositoryDisplayList.isEmpty() ? View.VISIBLE : View.GONE);
        mRecyclerView.setVisibility(repositoryDisplayList.isEmpty() ? View.GONE : View.VISIBLE);
        showToast(getString(R.string.search_repository_n_results, repositoryDisplayList.size()));
        mAdapter = new SearchRepositoryAdapter(getContext(), repositoryDisplayList, this);
        mLayoutManager = new GridLayoutManager(getContext(), 2, GridLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.search_menu, menu);
        // Retrieve the SearchView and plug it into SearchManager
        searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        searchManager = (SearchManager) getActivity().getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        searchView.setOnQueryTextListener(this);
        searchView.setIconifiedByDefault(true);
        searchView.setQueryHint("Search Here");
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        if (TextUtils.isEmpty(query)) {
            showToast(getString(R.string.repository_empty_query));
        } else if (query.length() < 3) {
            showToast(getString(R.string.repository_3_chars));
        } else {
            presenter.requestRepositoryData(query);
            emptyListCard.setVisibility(View.GONE);
        }
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    @Override
    public void onItemUrlClick(String url) {
        showCustomDialog(getString(R.string.external_url), getString(R.string.carry_on), getString(R.string.back), (dialogInterface, i) -> {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(browserIntent);
        }, null);
    }
}
