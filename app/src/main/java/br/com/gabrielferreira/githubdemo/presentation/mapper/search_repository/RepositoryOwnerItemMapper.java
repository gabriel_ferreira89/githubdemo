package br.com.gabrielferreira.githubdemo.presentation.mapper.search_repository;

import android.content.res.Resources;

import javax.inject.Inject;

import br.com.gabrielferreira.githubdemo.domain.entity.search_repository.RepositoryOwner;
import br.com.gabrielferreira.githubdemo.presentation.mapper.BaseDisplayMapper;
import br.com.gabrielferreira.githubdemo.presentation.model.display.search_repository.RepositoryOwnerDisplay;


class RepositoryOwnerItemMapper extends BaseDisplayMapper {

    @Inject
    RepositoryOwnerItemMapper(Resources resources) {
        super(resources);
    }


    RepositoryOwnerDisplay transform(RepositoryOwner owner){
        return new RepositoryOwnerDisplay()
                .withAvatarUrl(owner.getAvatarUrl())
                .withId(owner.getId())
                .withUrl(owner.getUrl());
    }

}
