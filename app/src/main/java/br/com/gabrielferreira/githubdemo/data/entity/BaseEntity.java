package br.com.gabrielferreira.githubdemo.data.entity;

public class BaseEntity {

    protected CodeDescriptionEntity status;

    protected CodeDescriptionEntity error;

    public CodeDescriptionEntity getStatus() {
        return status;
    }

    public CodeDescriptionEntity getError() {
        return error;
    }
}
