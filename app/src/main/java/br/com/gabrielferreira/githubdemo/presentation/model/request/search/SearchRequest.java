package br.com.gabrielferreira.githubdemo.presentation.model.request.search;

import com.google.gson.annotations.SerializedName;

public class SearchRequest {

    @SerializedName("q")
    private String searchKeywords;

    @SerializedName("sort")
    private SortType sort;

    @SerializedName("order")
    private OrderType order = OrderType.ASCENDING;

    public String getSearchKeywords() {
        return searchKeywords;
    }

    public SearchRequest withSearchKeywords(String searchKeywords) {
        this.searchKeywords = searchKeywords;
        return this;
    }

    public SortType getSort() {
        return sort;
    }

    public SearchRequest withSort(SortType sort) {
        this.sort = sort;
        return this;
    }

    public OrderType getOrder() {
        return order;
    }

    public SearchRequest withOrder(OrderType order) {
        this.order = order;
        return this;
    }
}
