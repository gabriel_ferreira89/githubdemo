package br.com.gabrielferreira.githubdemo.presentation.view.activity;

import android.content.DialogInterface;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import br.com.gabrielferreira.githubdemo.R;
import br.com.gabrielferreira.githubdemo.presentation.AndroidApplication;
import br.com.gabrielferreira.githubdemo.presentation.internal.component.ApplicationComponent;
import br.com.gabrielferreira.githubdemo.presentation.view.BaseView;
import butterknife.BindView;

public class BaseActivity extends AppCompatActivity implements BaseView {

    @Nullable
    @BindView(R.id.view_group_progress)
    protected ViewGroup mProgress;

    @Nullable
    @BindView(R.id.toolbar)
    protected Toolbar mToolbar;

    @Nullable
    @BindView(R.id.title_text_view)
    protected TextView mActionBarTitle;

    protected ActionBar mActionBar;
    private AlertDialog alertDialog;

    public ApplicationComponent getApplicationComponent() {
        return ((AndroidApplication) getApplication()).getApplicationComponent();
    }

    public void setupActionBar() {
        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
            mActionBar = getSupportActionBar();
            if (mActionBar != null) {
                mActionBar.setTitle("");
            }
            if (mActionBarTitle != null) {
                mActionBarTitle.setText(this.getTitle());
            }
        }
    }

    @Override
    public void showToast(int resId) {
        Toast.makeText(this, resId, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoading() {
        if (mProgress != null) {
            mProgress.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideLoading() {
        if (mProgress != null) {
            mProgress.setVisibility(View.GONE);
        }
    }

    @Override
    public void showCustomDialog(String msg, String positiveMsg, String negativeMsg,
                                 DialogInterface.OnClickListener onClickListener,
                                 DialogInterface.OnClickListener negativeOnClickListener) {
        if (TextUtils.isEmpty(msg)) {
            return;
        }
        if (alertDialog != null) {
            alertDialog.dismiss();
            alertDialog = null;
        }
        alertDialog = new android.support.v7.app.AlertDialog.Builder(this, R.style.AlertDialogStyle)
                .setMessage(msg)
                .setPositiveButton(positiveMsg, onClickListener)
                .setNegativeButton(negativeMsg, negativeOnClickListener)
                .show();
        if (alertDialog != null) {
            alertDialog.getButton(android.support.v7.app.AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.white));
        }
    }

    public void setTitle(String title) {
        if (mActionBarTitle != null) {
            mActionBarTitle.setText(title);
        }
    }
}
