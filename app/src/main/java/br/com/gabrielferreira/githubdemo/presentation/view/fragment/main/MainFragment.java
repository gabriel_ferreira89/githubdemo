package br.com.gabrielferreira.githubdemo.presentation.view.fragment.main;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.roughike.bottombar.BottomBar;

import br.com.gabrielferreira.githubdemo.R;
import br.com.gabrielferreira.githubdemo.presentation.view.fragment.BaseFragment;
import br.com.gabrielferreira.githubdemo.presentation.view.fragment.home.HomeFragment;
import br.com.gabrielferreira.githubdemo.presentation.view.fragment.search_repository.SearchRepositoryFragment;
import br.com.gabrielferreira.githubdemo.presentation.view.fragment.search_user.SearchUserFragment;
import br.com.gabrielferreira.githubdemo.presentation.view.fragment.settings.SettingsFragment;
import butterknife.BindView;
import butterknife.ButterKnife;

import static br.com.gabrielferreira.githubdemo.presentation.view.util.Constants.FragmentTag.TAG_HOME;
import static br.com.gabrielferreira.githubdemo.presentation.view.util.Constants.FragmentTag.TAG_REPOSITORY;
import static br.com.gabrielferreira.githubdemo.presentation.view.util.Constants.FragmentTag.TAG_SETTINGS;
import static br.com.gabrielferreira.githubdemo.presentation.view.util.Constants.FragmentTag.TAG_USER;

public class MainFragment extends BaseFragment {

    @BindView(R.id.main_bottomBar)
    BottomBar mBottomBar;

    private int containerResId;

    private String[] mFragmentTags = {
            TAG_HOME,
            TAG_REPOSITORY,
            TAG_USER,
            TAG_SETTINGS
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, view);
        getApplicationComponent().inject(this);
        setupActionBar();
        containerResId = R.id.fragment_container;
        setFragments();
        initmBottomBar();
        return view;
    }

    private void initmBottomBar() {
        mBottomBar.setOnTabSelectListener(tabId -> {
            switch (tabId) {
                case R.id.menu_home:
                    setTitle("Home");
                    showFragment(TAG_HOME);
                    break;
                case R.id.menu_repository:
                    setTitle("Repository");
                    showFragment(TAG_REPOSITORY);
                    break;
                case R.id.menu_user:
                    setTitle("User");
                    showFragment(TAG_USER);
                    break;
                case R.id.menu_settings:
                default:
                    setTitle("Settings");
                    showFragment(TAG_SETTINGS);
            }
        });
    }

    private void setFragments() {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(containerResId, new HomeFragment(), TAG_HOME);
        transaction.commitNow();
    }

    public void showFragment(String fragmentTag) {
        Fragment fragment;
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        for (String tag : mFragmentTags) {
            fragment = fragmentManager.findFragmentByTag(tag);
            if (fragment != null) {
                transaction.hide(fragment);
            }
        }
        fragment = fragmentManager.findFragmentByTag(fragmentTag);
        if (fragment != null) {
            transaction.show(fragment);
        } else if (TAG_HOME.equals(fragmentTag)) {
            transaction.add(containerResId, new HomeFragment(), TAG_HOME);
        } else if (TAG_REPOSITORY.equals(fragmentTag)) {
            transaction.add(containerResId, new SearchRepositoryFragment(), TAG_REPOSITORY);
        } else if (TAG_USER.equals(fragmentTag)) {
            transaction.add(containerResId, new SearchUserFragment(), TAG_USER);
        } else if (TAG_SETTINGS.equals(fragmentTag)) {
            transaction.add(containerResId, new SettingsFragment(), TAG_SETTINGS);
        }
        transaction.commitNowAllowingStateLoss();
    }
}
